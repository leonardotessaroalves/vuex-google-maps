import Vue from 'vue'
import Vuex from 'vuex'

import googleMaps from '@/components/google-maps'
import Play from './Play.vue'

const configs = {
  productionTip: false,  
  performance: true,
  devtools: true
}

Object.assign(Vue.config, configs)

Vue.use(Vuex)

const store = new Vuex.Store({  
  strict: true
})

Vue.use(googleMaps, {
  store: store,
  key: 'AIzaSyDBLQ7TMfwbOm7jCo0lF7W8VywvxbBuIh0',
  libraries: 'places, drawing'
})

new Vue({
  store,
  render: h => h(Play)
}).$mount('#play')


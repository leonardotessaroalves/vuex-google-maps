export const MARKER_PROPS = {
  anchorPoint: {
    type:Object,
    default () {
      return {x:0, y:0 }
    }
  },
  crossOnDrag: {
    type: Boolean,
    default: true
  },
  animation: {
    type: Number
  },
  clickable: {
    type: Boolean,
    default: true
  },
  cursor: {
    type: String
  },
  draggable: {
    type: Boolean,
    default: false
  },
  icon: {
    type: [String]
  },
  label: {
    type: String
  },
  opacity: {
    type: Number,
    default: 1
  },
  options: {
    type: Object
  },
  optimized: {
    type: Boolean,
    default: true
  },
  position: {
    type: Object,
    twoway: true
  },
  shape: {
    type: Object
  },
  title: {
    type: String
  },
  visible: {
    type: Boolean,
    default: true
  },
  zIndex: {
    type: Number
  }
}
export const MAP_OPTION_PROPS = {
  backgroundColor: {
    type: String,
  },
  center: {
    required: true,
    type: Object
  },
  clickableIcon: {
    type: Boolean,
    default: true
  },
  disableDefaultUI: {
    type: Boolean,
    default: false
  },
  disableDoubleClickZoom: {
    type: Boolean,
    default: true
  },
  draggable: {
    type: Boolean,
    default: true
  },
  draggableCursor: {
    type: String
  },
  draggingCursor: {
    type: String
  },
  gestureHandling: {
    type: String,
    default: 'auto'
  },
  heading: {
    type: Number
  },
  mapTypeControl: {
    type: Boolean,
    default: true
  },
  maxZoom: {
    type: Number,
    default: 20
  },
  minZoom: {
    type: Number,
    default: 0
  },
  noClear: {
    type: Boolean,
    default: false
  },
  panControl: {
    type: Boolean
  },
  panControlOptions: {
    type: Object
  },
  rotateControl: {
    type: Boolean
  },
  scaleControl: {
    type: Boolean
  },
  styles: {
    type: Array
  },
  tilt: {    
    type: Number,
    default: 0
  },
  zoom: {    
    type: Number,
    default: 8
  },
  zoomControl: {    
    type: Boolean
  }
}

export const MAP_PROPS = {
  ...MAP_OPTION_PROPS
}

const state = {
  maps: [],
  $G: null
}

const actions = {
  SET_PROMISE: ({ commit }, p) => {
    commit('MAP_PROMISE', p)
  }
}

const mutations = {
  API_KEY: (state, a) => {
    state.apiKey = a
  },
  LIBRARIES: (state, l) => {
    state.libraries = l
  },
  SET_CLASS: (state, c) => {
    state.$G = c
  },
  MAP_PROMISE: (state, p) => {
    state.classPromise = new Promise (async (resolve) => {
      await p.then((g) =>{
        resolve(g)
      })
    })
  }
}

const getters = {
  CLASS_PROMISE (state) {
    return state.classPromise
  },
  GET_G (state) {
    return state.$G
  }
}

const GOOGLE_VUEX = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}

export default GOOGLE_VUEX

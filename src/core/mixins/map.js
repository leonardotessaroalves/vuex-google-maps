import mapService from '@/core/mixins/service'
import { MAP_PROPS } from '@/core/utils/constants'

const events = [
  'bounds_changed',
  'click',
  'dblclick',
  'drag',
  'dragend',
  'dragstart',
  'idle',
  'mousemove',
  'mouseout',
  'mouseover',
  'resize',
  'rightclick',
  'tilesloaded',
]

export default {
  mixins: [
    mapService
  ],
  props: {
    ...MAP_PROPS
  },
  data () {
    return {
      local: {...this.$props}
    }
  },
  provide () {
    let self = this
    self.$map = new Promise(async (resolve) => {
      await self.$googlePromise.then((gm) => {

        const map = new gm.Map(self.$refs.e, {
          ...self.local,
          center: self.center,
          zoom: self.zoom
        })

        resolve(map)
      })
    })
    return {
      $mapInjector: self.$map
    }
  },
  created () {
    let self = this
    self.$map.then((map) => {
      self.$map = map

      for (let e of events) {          
        self.$map.addListener(e, (ev) => {
          self.$emit(e, ev)
        })
      }

      Object.keys(self.$props).forEach((key) => {
        const constructor = Object.getPrototypeOf(self.$map)
        const setMethodName = `set${key.cap()}`
        const getMethodName = `get${key.cap()}`
        const eventName = key.toLowerCase() + '_changed'        

        if (constructor.hasOwnProperty(setMethodName) === false) {            
          return
        } else if (setMethodName !== 'setStreetView') {
          // WATCH PROPERTIES
          self.$watch(() => self.$props[key], (val) => {
            self.$map[setMethodName](val)
          }, {
            deep: true,
            immediate: false
          })

          self.$map.addListener(eventName, () => {
            const value = self.$map[getMethodName]()
            self.local[key] = value.toJSON()
          })
        }
      })
    })
  },
  methods: {}
}
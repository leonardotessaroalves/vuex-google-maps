import { mapGetters } from 'vuex'

export default {
  props: {},
  data () {
    return {}
  },
  created () {
    let self = this
    self.$googlePromise.then((gm) => {
      self.$store.commit('GOOGLE/SET_CLASS', gm)
    })
  },
  computed: {
    ...mapGetters({
      $googlePromise: 'GOOGLE/CLASS_PROMISE',
      $G: 'GOOGLE/GET_G'
    })
  }
}
import mapService from '@/core/mixins/service'

const events = [
  'click',
  'rightclick',
  'dblclick',
  'drag',
  'dragstart',
  'dragend',
  'mouseup',
  'mousedown',
  'mouseover',
  'mouseout'
]

String.prototype.cap = function() {
  return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase() })
}

export default {
  mixins: [
    mapService    
  ],
  inject: ['$mapInjector'],
  data () {
    return {
      local: {...this.$props}
    }
  },
  mounted () {
    let self = this
    self.$refs['e'].remove()
  },
  created () {
    let self = this
    if (Promise.resolve(self.$mapInjector) === self.$mapInjector) {
      self.$mapInjector.then((map) => {
        self.$mapInstance = map

        self.$mapElement = new self.$G.Marker({
          ...self.local,
          map: map
        })

        for (let e of events) {          
          self.$mapElement.addListener(e, (ev) => {
            self.$emit(e, ev)
          })
        }

        Object.keys(self.$props).forEach((key) => {
          const constructor = Object.getPrototypeOf(self.$mapElement).constructor.Eb
          const setMethodName = `set${key.cap()}`
          const getMethodName = `get${key.cap()}`
          const eventName = key.toLowerCase() + '_changed'

          if (constructor.hasOwnProperty(setMethodName) === false) {            
            return
          } else {

            self.$mapElement.addListener(eventName, () => {
              const value = self.$mapElement[getMethodName]()
              self.local[key] = value.toJSON()
              self.$emit(eventName, value.toJSON())
            })
          }
        })
      })
    }
  },
  beforeDestroy () {
    let self = this
    self.$mapElement.setMap(null)
  }
}
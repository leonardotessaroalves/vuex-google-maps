import Gmap from './src/main'
import GmapMarker from './src/marker'
import GLOADER from 'load-google-maps-api'
import GStore from '@/core/store'

export default {
  install (Vue, Options = {}) {

    Options = {
      store: Options.store || null,
      ...Options
    }

    if (!Options.store) {
      // eslint-disable-next-line
      console.warn('You must provide store!')
      return
    }

    if (!Options.key) {
      // eslint-disable-next-line
      console.warn('You must provide Google Api Key!')
      return
    }

    if (!Options.libraries) {
      // eslint-disable-next-line
      console.warn('You must provide witch libraries you want to load!')
      return
    }
    
    const LOAD_MAP = GLOADER({ key: Options.key, libraries: Options.libraries.split(',')})    

    if (!Options.store.state.GOOGLE) {
      Options.store.registerModule('GOOGLE', { ...GStore, state: { ...GStore.state, classPromise: LOAD_MAP } })
      LOAD_MAP.then((g) => {
        Options.store.dispatch('GOOGLE/SET_GOOGLE_API', g)
        Options.store.dispatch('GOOGLE/SET_PROMISE', LOAD_MAP)
        Options.store.commit('GOOGLE/API_KEY', Options.key)
        Options.store.commit('GOOGLE/LIBRARIES', Options.libraries)
        }).catch((error) => {
          // eslint-disable-next-line
          console.warn(error)
        })
        
        Vue.component(Gmap.name, Gmap)
        Vue.component(GmapMarker.name, GmapMarker)
    }

  }
}

